## Dataset 9: Viral concentration of different genomic segments (Emaraviruses on Pistacio)

### Introduction to the dataset

The real dataset is composed of *Pistacia emaravirus B* (PiVB), a newly discovered Emaravirus from pistachio tree [(Buzkan et al., 2019)](https://www.sciencedirect.com/science/article/abs/pii/S0168170218306373). **The limit tested is the ability to detect all the genomic segments with different concentrations**. The viral genome is composed of seven distinct negative-sense, single-stranded RNAs, showing different frequencies in the dataset.

The different labs that analyzed the dataset are listed in Table 1.

*Table 1: Participants to the VIROMOCK challenge of Dataset 9.*

| Participant                      	| Institute 	| Country 	| email                                  	|
|----------------------------------	|-----------	|---------	|----------------------------------------	|
| Lucie Tamisier                   	| ULg       	| Belgium 	| <lucie.tamisier@uliege.be>             	|
| Annelies Haegeman, Yoika Foucart 	| ILVO      	| Belgium 	| <annelies.haegeman@ilvo.vlaanderen.be> 	|


The observed values of the different participants are listed in Table 2.

*Table 2: Observed composition of Dataset 9 after analysis by different labs.*

| Institute/Lab | Virus/viroid | Observed   closest NCBI accession | Observed   proportion of PiVB fragment reads (%)<sup>1</sup>  | Were   you able to detect all PiVB fragments? |
|:-------------:|--------------|-----------------------------------|--------------------------------------------------|-----------------------------------------------|
|      ILVO     | RNA1         | MH727572                          | 27                                               | yes                                           |
|      ILVO     | RNA2         | MH727573                          | 8.2                                              |                                               |
|      ILVO     | RNA3         | MH727574                          | 15.2                                             |                                               |
|      ILVO     | RNA4         | MH727575                          | 16.1                                             |                                               |
|      ILVO     | RNA5a        | MH727576                          | 8.3                                              |                                               |
|      ILVO     | RNA5b        | MH727577                          | 7.7                                              |                                               |
|      ILVO     | RNA6         | MH727578                          | 9.8                                              |                                               |
|      ILVO     | RNA7         | MH727579                          | 7.6                                              |                                               |

<sup>1</sup> This number represents the number of virus filtered reads mapped for each genomic segment against the total number of virus filtered reads.


### Comments of different labs while analyzing the dataset

Here you can read some comments the participants had while analyzing the dataset.

No comments yet.

### How to participate to the VIROMOCK challenge

The dataset can be downloaded [here](https://datadryad.org/stash/share/aw9JwkKUL9IoOi77IqNGAMWhkqjbbtSNwybqev_P968).

If you finish your analysis, we encourage you to submit your results through [this Google Sheet](https://docs.google.com/spreadsheets/d/1Io3NfUrBRQTv1G0CL8MxDhslpJzNJrgldrXHngfq1MU/edit#gid=0).

The Google Sheet will allow you to share your results in detail. Only the green columns are required. However, we encourage you to give as much information as possible.

After submission of the Google Sheet, your results will be processed and added to Table 2.
