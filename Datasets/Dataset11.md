## Dataset 11: *Pepino mosaic virus* (PepMV)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/nDw4EZdQ2uI5b5qU-KMN1x-HyZqUsHReQpVEw7jkoUM).

*Table 1: Composition of dataset 11. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     DQ000985      	|     Artificial    	|     6412                  	|     38766                               	|     1789                                              	|     39.90                                     	|
|     AJ606359      	|     Artificial    	|     6450                  	|     14878                               	|     687                                               	|     15.32                                     	|
|     MF422616      	|     Artificial    	|     6408                  	|     14532                               	|     670                                               	|     14.96                                     	|
|     JQ314460      	|     Artificial    	|     6414                  	|     9660                                	|     445                                               	|     9.94                                      	|
|     MK133092      	|     Artificial    	|     6412                  	|     9660                                	|     445                                               	|     9.94                                      	|
|     HG313807      	|     Artificial    	|     6408                  	|     9660                                	|     445                                               	|     9.94                                      	|



*Table 2: Percentage identity between the strains.*

|                       	|     AJ606359.1    	|     DQ000985.1    	|     HG313807.1    	|     JQ314460.1    	|     MF422616.1    	|     MK133092.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **AJ606359.1**    	|     100           	|     78.906        	|     81.566        	|     82.054        	|     95.57         	|     79.077        	|
|     **DQ000985.1**    	|     78.906        	|     100           	|     78.287        	|     78.322        	|     78.985        	|     98.519        	|
|     **HG313807.1**    	|     81.566        	|     78.287        	|     100           	|     86.558        	|     81.779        	|     78.492        	|
|     **JQ314460.1**    	|     82.054        	|     78.322        	|     86.558        	|     100           	|     82.236        	|     78.435        	|
|     **MF422616.1**    	|     95.57         	|     78.985        	|     81.779        	|     82.236        	|     100           	|     79.156        	|
|     **MK133092.1**    	|     79.077        	|     98.519        	|     78.492        	|     78.435        	|     79.156        	|     100           	|
