## Dataset 15: *Eggplant mottled dwarf virus* (EMDV)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/8cHuECHdPWcz9Xi29xAkqM22gWvAnTSvmoOjVp5XGrc).

*Table 1: Composition of dataset 15. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     LN680656      	|     Artificial    	|     13100                 	|     48460                               	|     1110                                              	|     49.95                                     	|
|     FR751552      	|     Artificial    	|     13093                 	|     24274                               	|     556                                               	|     25.02                                     	|
|     KJ082087      	|     Artificial    	|     13100                 	|     24274                               	|     556                                               	|     25.02                                     	|                                            	|     9.91                                      	|


*Table 2: Percentage identity between the strains.*

|                       	|     FR751552.4    	|     KJ082087.1    	|     LN680656.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|
|     **FR751552.4**    	|     100           	|     85.574        	|     85.658        	|
|     **KJ082087.1**    	|     85.574        	|     100           	|     97.74         	|
|     **LN680656.1**    	|     85.658        	|     97.74         	|     100           	|
