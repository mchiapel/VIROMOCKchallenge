## Dataset 18: *Barley yellow dwarf virus* (BYDV)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/campNN6N0iKlBWnntKUyj-nt51gJId_I3qpcm9f9ses).

*Table 1: Composition of dataset 18. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     EF521843      	|     Artificial    	|     5665                  	|     29194                               	|     1537                                              	|     31.11                                     	|
|     KF523382      	|     Artificial    	|     5686                  	|     14616                               	|     769                                               	|     15.58                                     	|
|     KY593456      	|     Artificial    	|     5671                  	|     14616                               	|     769                                               	|     15.58                                     	|
|     D11028        	|     Artificial    	|     5273                  	|     13826                               	|     728                                               	|     14.73                                     	|
|     KC559092      	|     Artificial    	|     4625                  	|     11850                               	|     624                                               	|     12.63                                     	|
|     EU332308      	|     Artificial    	|     5664                  	|     9732                                	|     512                                               	|     10.37                                     	|

*Table 2: Percentage identity between the strains.*

|                     	|     KY593456    	|     KF523382    	|     KC559092    	|     EU332308    	|     EF521843    	|     D11028    	|
|---------------------	|-----------------	|-----------------	|-----------------	|-----------------	|-----------------	|---------------	|
|     **KY593456**    	|     100         	|     74.266      	|     65.63       	|     78.133      	|     85.998      	|     72.954    	|
|     **KF523382**    	|     74.266      	|     100         	|     64.629      	|     71.144      	|     79.806      	|     89.248    	|
|     **KC559092**    	|     65.63       	|     64.629      	|     100         	|     64.663      	|     66.24       	|     65.144    	|
|     **EU332308**    	|     78.133      	|     71.144      	|     64.663      	|     100         	|     75.96       	|     71.069    	|
|     **EF521843**    	|     85.998      	|     79.806      	|     66.24       	|     75.96       	|     100         	|     81.667    	|
|     **D11028**      	|     72.954      	|     89.248      	|     65.144      	|     71.069      	|     81.667      	|     100       	|
