## Dataset 12: *Cassava mosaic virus* (CMV)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/gRUEa7B9Q-qBcw8Z8AQ47GiyxuPBrCbWyE-AwJ-07oE).

*Table 1: Composition of dataset 12. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     HE979770      	|     Artificial    	|     2799                  	|     48222                               	|     5167                                              	|     50.00                                     	|
|     HE979758      	|     Artificial    	|     2781                  	|     19278                               	|     2065                                              	|     19.98                                     	|
|     AJ314739      	|     Artificial    	|     2739                  	|     14472                               	|     1550                                              	|     15.01                                     	|
|     KR611579      	|     Artificial    	|     2746                  	|     14472                               	|     1550                                              	|     15.01                                     	|



*Table 2: Percentage identity between the strains.*

|                       	|     AJ314739.1    	|     HE979758.1    	|     HE979770.1    	|     KR611579.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **AJ314739.1**    	|     100           	|     71.113        	|     69.663        	|     83.863        	|
|     **HE979758.1**    	|     71.113        	|     100           	|     74.254        	|     72.734        	|
|     **HE979770.1**    	|     69.663        	|     74.254        	|     100           	|     68.192        	|
|     **KR611579.1**    	|     83.863        	|     72.734        	|     68.192        	|     100           	|
