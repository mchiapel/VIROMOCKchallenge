## Dataset 16: *Bell pepper endornavirus* (BPEV)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/UOv-uqGtu7ckKQiztr-CRUEzpa_cTJ6BaCYPMEFLU7o).

*Table 1: Composition of dataset 16. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     KX977568      	|     Artificial    	|     14727                 	|     49980                               	|     1020                                              	|     50                                        	|
|     KR080326      	|     Artificial    	|     14729                 	|     19992                               	|     408                                               	|     20                                        	|
|     JN019858      	|     Artificial    	|     14728                 	|     14994                               	|     306                                               	|     15                                        	|
|     JQ951943      	|     Artificial    	|     14727                 	|     14994                               	|     306                                               	|     15                                        	|                                              	|     9.91                                      	|


*Table 2: Percentage identity between the strains.*

|                       	|     JN019858.1    	|     JQ951943.1    	|     KR080326.1    	|     KX977568.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **JN019858.1**    	|     100           	|     87.97         	|     71.895        	|     88.209        	|
|     **JQ951943.1**    	|     87.97         	|     100           	|     71.872        	|     99.063        	|
|     **KR080326.1**    	|     71.895        	|     71.872        	|     100           	|     71.915        	|
|     **KX977568.1**    	|     88.209        	|     99.063        	|     71.915        	|     100           	|
